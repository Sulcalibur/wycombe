var gulp          = require('gulp'),
    jade          = require('gulp-jade'),
    prettify      = require('gulp-prettify'),
    sass          = require('gulp-sass'),
    minifyCSS     = require('gulp-minify-css'),
    uglify        = require('gulp-uglify'),
    browserSync   = require('browser-sync')
    imagemin      = require('gulp-imagemin'),
    evilIcons     = require("gulp-evil-icons"),
    pngquant      = require('imagemin-pngquant'),
    concat        = require('gulp-concat'),
    concatCss     = require('gulp-concat-css'),
    sassdoc       = require('gulp-sassdoc'),
    glob          = require('glob'),
    uncss         = require('gulp-uncss'),
    rimraf        = require('rimraf'),
    // autoprefix = require('gulp-autoprefixer'),
    // notify     = require('gulp-notify'),
    // markdown   = require('gulp-markdown'),
    // bower      = require('gulp-bower'),
    watch         = require('gulp-watch')
    ;

  // Markup Tasks
  gulp.task('markup', function() {
    gulp.src('project/*.jade')
    .pipe(jade())
    .pipe(prettify({indent_size: 2}))
    .pipe(gulp.dest('build/'));
  });

  gulp.task('sassdoc', function () {
    return gulp
      .src('project/assets/styles')
      .pipe(sassdoc({
        'dest': 'docs',
        'config': 'docs'
      }));
  });

  // gulp.task('sass', function () {
  //   gulp.src('project/assets/*.scss')
  //   .pipe(sass())
  //   .pipe(gulp.dest('build/css'));
  // });

  // // JavaScript Tasks
  // gulp.task('scripts', function(){
  //   gulp.src('js/*.js')
  //   // gulp.src('bower_components/modernizr/modernizr.js')
  //   //.pipe(plumber())
  //   .pipe(uglify())
  //   .pipe(gulp.dest('build/js/'))
  // });

  // JavaScript Tasks
  gulp.task('scriptspre', function(){
    gulp.src('bower_components/modernizr/modernizr.js')
    gulp.src('bower_components/jquery/dist/jquery.js')
    // .pipe(uglify())
    // .pipe(concat('jspre.js'))
    .pipe(gulp.dest('build/js/'))
  });

  // JavaScript Tasks
  gulp.task('scriptspost', function(){
    gulp.src('project/assets/scripts/*.js')
    // .pipe(uglify())
    .pipe(concat('jspost.js'))
    .pipe(gulp.dest('build/js/'))
  });

  // Images Tasks
  gulp.task('images', function(){
    gulp.src('project/assets/images/*')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('build/img/'));
  });

  // Styles Tasks
  gulp.task('styles', function() {
    gulp.src('project/assets/styles/*.scss')
    .pipe(sass({
      style: 'compress'
    }))
    // .pipe(uglify())
    // gulp.src('node_modules/evil-icons/assets/css/evil-icons.css')
    .pipe(concat('style.css'))
    .pipe(minifyCSS({keepBreaks:true}))
    .pipe(gulp.dest('build/css/'))
  });

  gulp.task('browser-sync', function () {
    var files = [
    'build/**/*.html',
    'build/css/**/*.css',
    'build/img/**/*.svg',
    'build/img/**/*.jpg',
    'build/img/**/*.gif',
    'build/img/**/*.png',
    'build/js/**/*.js'
    ];

    browserSync.init(files, {
      server: {
        baseDir: './build'
      }
    });
  });

  // // CSS Clean Up
  // gulp.task('cssclean', function() {
  //     gulp.src('build/css/style.css')
  //       .pipe(uncss({
  //           html: glob.sync('build/**/*.html')
  //       }))
  //       .pipe(gulp.dest('build/css'));
  //     gulp.src('build/css/style.css')
  //       .pipe(minifyCSS({keepBreaks:true}))
  //       .pipe(gulp.dest('build/css/min/'))
  // });

  // Folder Clean Up
  gulp.task('purge', function (cb) {
      rimraf('build', cb);
  });

  gulp.task('build',
    ['purge','markup','styles','scriptspre','scriptspost','images']);

  // Watch Task
  gulp.task('watch', function() {
    gulp.watch('project/**/*.jade', ['markup']);
    gulp.watch('project/assets/styles/**/*.scss', ['styles']);
  });

  // Default task to be run with `gulp`
  gulp.task('default', ['markup','styles','scriptspre','scriptspost','watch','browser-sync']);
