
p We at Wycombe Recycling like to think we go that little hit extra with our service which is why we have the latest technology when delivering service to you. When placing orders you can automatically receive a text or email confirmation at receipt or payment and when our driver when he is on his way to you, you can automatically receive a text or email confirmation too. We are not a massive company but we do have the manpower to deal with your orders in a friendly and efficient manner. Please do not hesitate to contact us with any questions. 

h3 Current Standard Skip Hire Rates 

p Call us 01234 561890 or email inFo@wycomberecycling.co.uk

p Prices are subject to materials being disposed or and site location, please call to discuss.

p Please do not overfill the skips. They must he safe to transport. if you overfill the skip, please do not be offended if our driver asks you to take some items out. It is his licence at stake if he
picks up the load and it is unsafe.

h3 What size skip do you need?
p The above are the sizes and prices or our skips. Call us old fashioned but for some reason skip sizes are still measured in cubic yards! The 6 yard builders skip is the most common which is about the size of a small car. If you need more guidance on sizes, please just call.

p Be realistic about the size or the skip you require. Having to hire 2 small skips works out more
expensive than 1 large one. lr you really have a lot or ruhhish then it will work out cheaper to
use the service of our grah lorry which holds the equivalent to nearly three s yard skips &
saves on labour too.
where do you want your skip put?
Please he aware that skips cannot he put on puhlic roads without a permit, correct signage
and lights. This all adds to the expense so you might prerer ror us to deliver the skip and wait
for you to load it then take it away. This is called "wait & Load". vou get halran hour to load
the skip included in the norrnal price.
iryou must have your skip put on a puhlic road then we are arraid there are some additional
costs:
£30 administration fee ifyou want us to arrange the permit
£7??? whatever your local highways charge us ror the permit (usually hetween co & £35)
Arranging the permit can take up to lo working days so please ensure you hook in advance.
All skips on the roads need to have a light positioned on each corner and he surrounded hy
trarric cones. we can supply these rree or charge however there will he a £100 returnahle
deposit with the order and a £35 charge per item ror any that are missing on collection or the
skip.
Most people have the skip put on the driveway. Don't worry, we'll he carerul not to hlock the
garage doors! Please he aware though thatdue to the stahilising equipmenton the skip
lorries, although rare, damage to driveways can sometimes occur.
Access
The skip lorries require 2.7m access hut our lorries do have extendable arms with an
additional 2m reach. This means if they can not reverse right into the driveway we can still
drop the hin in the drive providing the access is clear.
what do you want to put in your skip?
Our skips are For general & inert waste only and must only he level loaded to ensure they can
he sarely transported.
Due to waste Regulations, you cannot put the following items in them:-
Plasterhoard must now he kept separate and dry otherwise a 30% handling charge will apply
to each skip round with plasterhoard mixed amongst the load. iryou don't have enough
plasterhoard to warrant an ektra skip you can put it in a seperate 1 ton hag. Place the hag on
top or the loaded skip when ready ror collection, still he carerul to ensure the skip remains
level loaded. The additional cost ror the plasterhoard is £50/bag if collected with the skip or
hetween £85 a. £105 (depending on how far you are rrom us) if collected seperately.
Tyres, ashestos, car hatteries, fridge-freezers, clinical waste, liquids, "smelly" waste, large
tree roots/logs, hazardous/corrosive suhstances, gas cylinders, oily wastes, televisions is
monitors, rluorescent tuhes, hatteries, or any other waste classed as hazardous.
wees (waste Electrical and Electronic Equipment)
we are ahle to accept wees (waste Electrical and Electronic Equipment) in our skips at an
additional cost oraround £15 per item. Please check with us rirst herore placing items in the
skip for an accurate quotation.
Get in touch today. Onec ex sapien, laoreet et magna sit amet, aliquet condimentum ante.
Call us 01234 567890 or email infu@wycomberecycling.co.uk
f I in
(ti copyright wycornhe Recycling Privacy i rerms

